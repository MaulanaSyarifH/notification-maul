package com.trainingmicroservicemaul.notification.dto;

import lombok.Data;

@Data
@SuppressWarnings("unused")
public class EmailNotification {
	private String to;
    private String from;
    private String subject;
    private String content;
}
